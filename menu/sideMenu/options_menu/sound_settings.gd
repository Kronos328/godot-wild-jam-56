extends TabBar

@export
var master_slider: HSlider
var master_index = AudioServer.get_bus_index("Master")

# Called when the node enters the scene tree for the first time.
func _ready():
	master_slider.value = db_to_linear(AudioServer.get_bus_volume_db(master_index)) 

func _on_master_value_changed(value):
	var value_db = linear_to_db(value)
	AudioServer.set_bus_volume_db(master_index, value_db)
	#print("Master", AudioServer.get_bus_volume_db(master_index))
	print("%s teste123" % value_db)
