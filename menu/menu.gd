extends Control

@export
var options_menu: Control

@export
var credits_menu: Control


func _ready():
	GameData.refresh_collected_items()


func _on_options_button_pressed():
	options_menu.show()


func _on_credits_button_pressed():
	credits_menu.show()


func _on_quit_button_pressed():
	get_tree().quit()


func _on_start_button_pressed():
	get_tree().change_scene_to_file("res://main.tscn")
