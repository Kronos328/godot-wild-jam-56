extends Node3D

var scare_locations: Array[ScareLocation] = []
var rng := RandomNumberGenerator.new()

func _ready():
	for child in get_children():
		if not child is ScareLocation:
			push_error("%s is not ScareLocation" % child)
			continue
		
		child = child as ScareLocation
		child.body_entered.connect(
			_on_scare_location_triggered.bind(child)
		)


func _on_scare_location_triggered(player: CharacterBody3D, scare_location: ScareLocation):
	var random_integer = rng.randi_range(1, 100)
	print("Rolled %s on ScareLocation %s" % [random_integer, scare_location.name])
	if random_integer >= scare_location.monster_appear:
		_trigger_monster_appear(scare_location)
	elif random_integer >= scare_location.scare_player:
		_trigger_scare_player(scare_location)


func _trigger_monster_appear(scare_location: ScareLocation):
	print("Monster triggered by player on %s" % scare_location.name)
	scare_location.trigger_monster_appear()
	


func _trigger_scare_player(scare_location: ScareLocation):
	print("Player Scare triggered by player on %s" % scare_location.name)
	scare_location.trigger_scare_ambience()
