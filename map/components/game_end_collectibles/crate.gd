extends InteractibleArea

var dialogue = preload("res://data/dialogues/crate_get.dialogue")


func _ready():
	visible = false
	set_collision_layer_value(3, false)


func request_interaction():
	DialogueManager.show_example_dialogue_balloon(dialogue)
	DialogueManager.dialogue_ended.connect(
		func (_res): 
			GameData.collect_blood()
			finish_interaction()
			queue_free(),
		CONNECT_ONE_SHOT
	)


func _on_syringe_get():
	visible = true
	set_collision_layer_value(3, true)

