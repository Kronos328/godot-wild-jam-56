extends InteractibleArea

var dialogue = preload("res://data/dialogues/syringe_get.dialogue")

func request_interaction():
	DialogueManager.show_example_dialogue_balloon(dialogue)
	DialogueManager.dialogue_ended.connect(
		func (_res): 
			GameData.collect_syringe()
			finish_interaction()
			queue_free(),
		CONNECT_ONE_SHOT
	)
