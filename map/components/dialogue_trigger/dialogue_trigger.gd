extends Node3D

@export
var dialogue: DialogueResource

@onready
var interactible_area: InteractibleArea = $InteractibleArea


func _on_interactible_area_interact():
	DialogueManager.show_example_dialogue_balloon(dialogue)
	DialogueManager.dialogue_ended.connect(
		func (_res): interactible_area.finish_interaction(),
		CONNECT_ONE_SHOT
	)
	
	
