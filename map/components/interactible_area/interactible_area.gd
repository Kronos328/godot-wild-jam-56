class_name InteractibleArea extends Area3D

signal interact

signal interaction_finished

@export
var preview_texture: Texture

func request_interaction():
	interact.emit()


func finish_interaction():
	interaction_finished.emit()
