extends Node3D

var success_dialogue = preload("res://data/dialogues/end_game_success.dialogue")
var check_dialogue = preload("res://data/dialogues/end_game_item_check.dialogue")

@onready
var interactible_area = $InteractibleArea

func _on_interactible_area_interact():
	if GameData.can_finish_game():
		DialogueManager.show_example_dialogue_balloon(success_dialogue)
		DialogueManager.dialogue_ended.connect(
			func (_res): 
				interactible_area.finish_interaction()
				get_tree().change_scene_to_file("res://game_end/game_end.tscn"),
			CONNECT_ONE_SHOT
		)
	else:
		DialogueManager.show_example_dialogue_balloon(check_dialogue)
		DialogueManager.dialogue_ended.connect(
			func (_res): 
				interactible_area.finish_interaction(),
			CONNECT_ONE_SHOT
		)

