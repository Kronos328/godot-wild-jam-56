class_name ScareLocation extends Area3D

@export
var cooldown_time_max: float

@export
var cooldown_time_min: float

@export_category("Trigger Thresholds")
@export_range(0, 100, 1)
var monster_appear: int

@export_range(0, 100, 1)
var scare_player: int

@onready
var cooldown_timer: Timer = $CooldownTimer

@onready
var scare_ambiences = $ScareAmbiences

@onready
var animation_player= $AnimationPlayer

func _ready():
	var cooldown_time = randf_range(cooldown_time_min, cooldown_time_max)
	cooldown_timer.wait_time = cooldown_time


func _on_cooldown_timer_timeout():
	set_deferred("monitoring", true)
	var cooldown_time = randf_range(cooldown_time_min, cooldown_time_max)
	cooldown_timer.wait_time = cooldown_time
	print("%s ScareLocation cooldown finished" % name)
	print("%s new cooldown is %s" % [name, cooldown_time])


func start_cooldown():
	set_deferred("monitoring", false)
	cooldown_timer.start()


func trigger_scare_ambience():
	scare_ambiences.get_children().pick_random().play()
	start_cooldown()


func trigger_monster_appear():
	animation_player.play("monster_attack")


func process_monster_attack():
	var overlapping_bodies = get_overlapping_bodies()
	
	if overlapping_bodies.is_empty():
		return
	
	overlapping_bodies.front().apply_monster_attack()
