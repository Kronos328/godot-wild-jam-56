extends Node3D

@export
var map_start_point: Marker3D

@export
var player: Player


func _ready():
	player.position = map_start_point.position
