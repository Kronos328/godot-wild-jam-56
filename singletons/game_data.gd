extends Node

var _syringe_collected = false
var _blood_collected = false
var _keycard_collected = false


func can_finish_game():
	return (
		_syringe_collected
		and _blood_collected
		and _keycard_collected
	)


func collect_syringe():
	_syringe_collected = true


func collect_blood():
	_blood_collected = true


func collect_keycard():
	_keycard_collected = true


func refresh_collected_items():
	_syringe_collected = false
	_blood_collected = false
	_keycard_collected = false
