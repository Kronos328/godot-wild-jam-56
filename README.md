# Blind in the Dark

A game made by students of UNIVALI's Game Design course as a challange using only Open-Source tools. Also, it was made as an entry to the Godot Wild Jam 56.

In it, you must explore a cave whilst running away from a supposed monster... But in reality, one may find that the true monster is elsewhere...

# Controls:

WASD - Movement

Left-click: Interact

# Other Links

[Itch.io Page](https://kronos328.itch.io/blind-in-the-dark)
