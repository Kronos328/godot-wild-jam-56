extends Control

@onready
var interact_label: TextureRect = $InteractLabel


func interact_prompt_display(custom_texture: Texture):
	interact_label.texture = custom_texture
	interact_label.show()


func interact_prompt_hide():
	interact_label.hide()
