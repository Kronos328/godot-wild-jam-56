extends State


func _after_enter():
	var current_interaction: InteractibleArea = target.get_current_interactible()
	
	current_interaction.request_interaction()
	await current_interaction.interaction_finished
	
	get_root().change_state_name("Idle")
