extends State

func _update(delta: float):
	var input_dir = Input.get_vector(
		"strafe_left",
		"strafe_right",
		"move_front",
		"move_back"
	)
	
	if input_dir == Vector2.ZERO:
		get_root().change_state_name("Idle")
	
	target.apply_movement(input_dir)
