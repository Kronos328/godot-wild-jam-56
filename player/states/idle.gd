extends State

func _update(delta: float):
	_check_movement()


func _check_movement():
	var input_dir = Input.get_vector(
		"strafe_left",
		"strafe_right",
		"move_front",
		"move_back"
	)
	
	if input_dir != Vector2.ZERO:
		get_root().change_state_name("Moving")
