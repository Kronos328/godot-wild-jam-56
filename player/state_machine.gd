extends State

func _input(event):
	_check_interaction(event)

func _check_interaction(event: InputEvent):
	if not target.can_interact():
		return
	
	if not event is InputEventMouseButton:
		return
	
	if not get_active_substate().name == "Idle":
		return
	
	if not event.is_action_pressed("interact"):
		return
	
	change_state_name("Interacting")
