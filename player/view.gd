class_name PlayerView extends Node3D

const LOOK_UPPER_BOUND = 40
const LOOK_LOWER_BOUND = -30
const LOOK_SENSITIVITY = 0.1


func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _input(event):
	if not event is InputEventMouseMotion:
		return
	event = event as InputEventMouseMotion
	var mouse_direction :Vector2 = event.relative
	rotation.x = clampf(
		rotation.x - mouse_direction.y * LOOK_SENSITIVITY,
		deg_to_rad(LOOK_LOWER_BOUND),
		deg_to_rad(LOOK_UPPER_BOUND)
	)
