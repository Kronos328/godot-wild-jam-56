class_name Player extends CharacterBody3D

const LOOK_SENSITIVITY = 0.001
const LOOK_UPPER_BOUND = 40
const LOOK_LOWER_BOUND = -50

@export
var speed := 5.0

@onready
var interaction_raycast: RayCast3D = $InteractionRaycast

@onready
var ui = $UI

@onready
var state_machine: State = $StateMachine

@onready
var animation_player = $AnimationPlayer

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _input(event):
	if event is InputEventMouseMotion:
		_process_mouse_body_rotation(event)


func _process_mouse_body_rotation(event: InputEventMouseMotion):
	if not _can_turn_camera():
		return
	
	var mouse_direction :Vector2 = event.relative
	rotation.x = clampf(
		rotation.x - mouse_direction.y * LOOK_SENSITIVITY,
		deg_to_rad(LOOK_LOWER_BOUND),
		deg_to_rad(LOOK_UPPER_BOUND)
	)
	rotation.y -= mouse_direction.x * LOOK_SENSITIVITY


func apply_movement(input_dir: Vector2):
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)
	
	move_and_slide()


func can_interact():
	var raycast_is_colliding = interaction_raycast.is_colliding()
	
	if raycast_is_colliding:
		ui.interact_prompt_display(interaction_raycast.get_collider().preview_texture)
	else :
		ui.interact_prompt_hide()
	
	return raycast_is_colliding


func get_current_interactible():
	ui.interact_prompt_hide()
	return interaction_raycast.get_collider()


func _can_turn_camera() -> bool:
	var current_state = state_machine.get_active_substate()
	
	if is_instance_valid(current_state):
		return not current_state.name in ["Interacting", "PerformingAnimation"]
	else:
		return true 


func apply_monster_attack():
	state_machine.change_state_name("PerformingAnimation")
	animation_player.animation_finished.connect(
		func(_x): state_machine.change_state_name("Idle"),
		CONNECT_ONE_SHOT
	)
	animation_player.play("monster_attack_hit")
	
