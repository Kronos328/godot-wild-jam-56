extends Node3D

@onready
var step_timer = $StepTimer

@onready
var step_sounds = $StepSounds

var step_sound_players: Array[AudioStreamPlayer3D] = []


func _ready():
	for child in step_sounds.get_children():
		step_sound_players.append(child)


func _on_moving_entered():
	step_timer.start()


func _on_moving_exited():
	step_timer.stop()


func _on_step_timer_timeout():
	randomize()
	step_sound_players.pick_random().play()
